`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:43:34 AM
// Design Name: 
// Module Name: TwoinEQ
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TwoinEQ #(parameter W =16)(
    input [W-1:0] in0,
    input [W-1:0] in1,
    output out
    );
    
    assign out = (in0 == in1)? 1:0;
endmodule