`timescale 1ns / 1ps
`include "RD_FF.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RD_FF_tb;
	reg clk, reset;
    reg[1:0] D;
    wire[1:0] F;
    RD_FF ff1(clk, reset, D, F);
    
    initial
    begin
    	$dumpfile("RD_FF_tb.vcd");
        $dumpvars(0, RD_FF_tb);
        clk = 1'b0;
    	D = 2'd2;
    	reset = 1'b0;
    end
    
    always
    begin
    	#2 clk = ~clk;
    end
    
    initial
    begin
    	#5 reset = 1'b1;
    	#5 $finish;
    end
endmodule
