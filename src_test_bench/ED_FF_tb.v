`timescale 1ns / 1ps
`include "ED_FF.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ED_FF_tb;
	reg clk, en;
    reg[15:0] D;
    wire[15:0] F;
    ED_FF ff1(clk, en, D, F);
    
    initial
    begin
    	$dumpfile("ED_FF_tb.vcd");
        $dumpvars(0, ED_FF_tb);
        clk = 1'b0;
    	D = 16'd5;
    	en = 0;
    end
    
    always
    begin
    	#2 clk = ~clk;
    end
    
    initial
    begin
    	#5 D = 16'd6;
    	en = 1;
    	#5 $finish;
    end
endmodule
