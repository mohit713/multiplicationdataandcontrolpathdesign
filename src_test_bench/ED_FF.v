`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:21:37 AM
// Design Name: 
// Module Name: RD_FF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ED_FF#(parameter W = 16)(
    input clk,
    input en,
    input[W-1:0] D, 
    output reg [W-1:0] F
    );
    
    always@(posedge clk)
    begin
        if(en)
            F <= D;
    end
endmodule
