`timescale 1ns / 1ps
`include "adder.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder_tb;
    reg[15:0] A, B;
    wire[15:0] F;
    adder a1(A, B, F);
    
    initial
    begin
    	
    	$dumpfile("adder_tb.vcd");
        $dumpvars(0, adder_tb);
    	A = 16'd5;
    	B = 16'd6;
    end
    
    initial
    begin
    	#5 $finish;
    end
endmodule
