`timescale 1ns / 1ps
`include "datapath.v"
`include "controlunit.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:00:36 AM
// Design Name: 
// Module Name: mult
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mult#(parameter W = 16)(
    input clk,
    
    // data signals
    input[W-1:0] operand_bits_A,
    input[W-1:0] operand_bits_B,
    output[W-1:0] result_bits_data,
    
    // control sugnals
    input input_available,
    input reset,
    output result_rdy,
    input result_taken
    );
    wire A_en, B_en, M_en, B_mux_sel, M_mux_sel, B_zero;
    datapath#(16) datapath(clk, operand_bits_A, operand_bits_B, result_bits_data, A_en, B_en, M_en, B_mux_sel, M_mux_sel, B_zero);
    controlunit#(16) control(clk, reset, input_available, result_rdy, result_taken, A_en, B_en, M_en, B_mux_sel, M_mux_sel, B_zero);
    
endmodule
