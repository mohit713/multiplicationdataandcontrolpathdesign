`timescale 1ns / 1ps
`include "RD_FF.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 04:37:27 AM
// Design Name: 
// Module Name: controlunit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module controlunit(
    input clk,
    input reset,
    
    // data signals
    input input_available,
    output reg result_rdy,
    input result_taken,
    
    // control signals ctrl -> data
    output reg A_en,
    output reg B_en,
    output reg M_en,
    output reg B_mux_sel,
    output reg M_mux_sel,
    
    // control signals data -> ctrl
    input B_zero
    );
    
    // local params are scoped constants
    localparam WAIT = 2'd0;
    localparam CALC = 2'd1;
    localparam DONE = 2'd2;
    
    // state register
    reg [1:0] state_next;
    wire [1:0] state;
    
    // state flipflop
    RD_FF state_ff(clk, reset, state_next, state);
    
    //output signal logic
    always@(*)
    begin
        // define control signals
        B_mux_sel = 1'b0;
        M_mux_sel = 1'b0;
        A_en = 1'b0;
        B_en = 1'b0;
        M_en = 1'b0;
        
        result_rdy = 1'b0;
        case(state)
            WAIT: 
            begin
                B_mux_sel = 1'b0;
                M_mux_sel = 1'b0;
                A_en = 1'b1;
                B_en = 1'b1;
                M_en = 1'b1;
            end
            CALC:
            begin
                if(!B_zero)
                begin
                    B_mux_sel = 1'b1;
                    M_mux_sel = 1'b1;
                    A_en = 1'b1;
                    B_en = 1'b1;
                    M_en = 1'b1;
                end    
            end
            DONE:
                result_rdy = 1'b1;
        endcase
    end
    
    // Next State Logic
    always@(*)
    begin
        state_next = state;
        case(state)
        WAIT:
            if(input_available)
                state_next = CALC;
        CALC:
            if(B_zero)
                state_next = DONE;
        DONE:
            if(result_taken)
                state_next = WAIT;
        endcase
    end
    
endmodule
