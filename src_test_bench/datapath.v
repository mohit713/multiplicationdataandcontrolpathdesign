`timescale 1ns / 1ps
`include "ED_FF.v"
`include "TwoinMUX.v"
`include "subtractor.v"
`include "adder.v"
`include "TwoinEQ.v"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/19/2019 12:20:25 PM
// Design Name: 
// Module Name: datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module datapath #(parameter W = 16)(
    input clk,
    // data signals
    input [W-1:0] operand_bits_A,
    input [W-1:0] operand_bits_B,
    output [W-1:0] result_bits_data,
    
    // control signal from ctrl->data
    input A_en,
    input B_en,
    input M_en,
    input B_mux_sel,
    input M_mux_sel,
    
    // control signal form data->ctrl
    output B_zero
    );
    // take input A using D-FlipFlop
    wire[W-1:0] A;
    ED_FF#(W) A_ff(clk, A_en, operand_bits_A, A);
    
    wire[W-1:0] sub_out;
    wire[W-1:0] B_mux_out;
    // mux for B-data
    TwoinMUX#(W) B_mux(operand_bits_B, sub_out, B_mux_sel, B_mux_out);
    // input B using D-FlipFlop
    wire[W-1:0] B;
    ED_FF#(W) B_ff(clk, B_en, B_mux_out, B);
    
    wire[W-1:0] add_out;
    wire[W-1:0] M_mux_out;
    // mux for M-data
    TwoinMUX#(W) M_mux(16'd0, add_out, M_mux_sel, M_mux_out);
    wire[W-1:0] M;
    // input M using D-FlipFlop
    ED_FF#(W) M_ff(clk, M_en, M_mux_out, M);
    
    // computation operation
    subtractor#(W) sub(B, 16'd1, sub_out);
    adder#(W) add(M, A, add_out);
    TwoinEQ#(W) B_eq_0(B, 16'd0, B_zero);
    
    assign result_bits_data = M;
endmodule
