`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:37:34 AM
// Design Name: 
// Module Name: TwoinMUX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TwoinMUX #(parameter W =16)(
    input [W-1:0] in0,
    input [W-1:0] in1,
    input mux_sel,
    output [W-1:0] out
    );
    
    assign out = (mux_sel==0)? in0:in1;
endmodule
