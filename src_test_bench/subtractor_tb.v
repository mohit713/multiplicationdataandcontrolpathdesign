`timescale 1ns / 1ps
`include "subtractor.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module subtractor_tb;
    reg[15:0] A, B;
    wire[15:0] F;
    subtractor s1(A, B, F);
    
    initial
    begin
    	$dumpfile("subtractor_tb.vcd");
        $dumpvars(0, subtractor_tb);
    	A = 16'd11;
    	B = 16'd1;
    end
    
    initial
    begin
    	#5 $finish;
    end
endmodule
