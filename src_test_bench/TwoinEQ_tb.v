`timescale 1ns / 1ps
`include "TwoinEQ.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TwoinEQ_tb;
    reg[15:0] A, B;
    wire F;
    TwoinEQ eq1(A, B, F);
    
    initial
    begin
    	
    	$dumpfile("TwoinEQ_tb.vcd");
        $dumpvars(0, TwoinEQ_tb);
    	A = 16'd5;
    	B = 16'd6;
    end
    
    initial
    begin
    	#5 B = 16'd5;
    	#5 $finish;
    end
endmodule
