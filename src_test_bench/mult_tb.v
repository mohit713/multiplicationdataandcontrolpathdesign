`timescale 1ns / 1ps
`include "mult.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mult_tb;
	reg clk;
    reg[15:0] A, B;
    wire[15:0] F;
    reg input_available;
    reg reset;
    wire result_rdy;
    reg result_taken;
    reg done;
    mult m1(clk, A, B, F, input_available, reset, result_rdy, result_taken);
    
    initial
    begin
    	$dumpfile("mult_tb.vcd");
        $dumpvars(0, mult_tb);
        done = 1'b0;
        clk = 1'b0;
    	A = 16'd8;
    	B = 16'd7;
    	input_available = 1'b0;
    	reset = 1'b1;
    	result_taken = 1'b0;
    end
    
    always
    begin
    	#2 clk = ~clk;
    end
    
    initial
    begin
    	#15 reset = 1'b0;
    	#10 input_available = 1'b1;
    	#5 input_available = 1'b0;
    	wait(done)
    	begin
    		#10 result_taken = 1'b1;
    		#10 $finish;
    	end    	
    end
    
    always@(*)
    begin
    	if(result_rdy == 1'b1)
    		done = 1'b1;
		else
			done = 1'b0;
    end
    
endmodule
