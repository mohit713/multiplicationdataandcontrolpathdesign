`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:21:37 AM
// Design Name: 
// Module Name: RD_FF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RD_FF(
    input clk,
    input reset,
    input[1:0] D, 
    output reg [1:0] F
    );
    
    always@(posedge clk)
    begin
        if(reset)
            F <= 0;
        else
            F <= D;
    end
endmodule
