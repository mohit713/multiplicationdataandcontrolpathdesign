`timescale 1ns / 1ps
`include "TwoinMUX.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2019 05:31:11 AM
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TwoinMUX_tb;
    reg[15:0] in0, in1;
    reg mux_sel;
    wire[15:0] F;
    TwoinMUX m1(in0, in1, mux_sel, F);
    
    initial
    begin
    	
    	$dumpfile("TwoinMUX_tb.vcd");
        $dumpvars(0, TwoinMUX_tb);
    	in0 = 16'd5;
    	in1 = 16'd6;
    	mux_sel = 1'b0;
    end
    
    initial
    begin
    	#5 mux_sel = 1'b1;
    	#5 $finish;
    end
endmodule
